'use strict';

// Stub the window so that it's ok for code that tests
// the existence of window.* things
global.window = {};
global.document = {
    addEventListener: function () {}
};

var test = require('tape');
var THREE = require('three');
var index = require('../build/index.common.js');

test('Test three', function (t) {
    t.ok(true, 'Can run a test');
    var vec = new THREE.Vector3(1,2,3);
    t.equal(vec.x,1,'Vector x');
    t.equal(vec.lengthSq(),1+4+9, 'Vector length');

    var camera = new THREE.PerspectiveCamera( 45, 1.0, 1, 1000 );
    t.equal(camera.far, 1000, 'Camera far');

    t.end();
});

test('Test three plugins', function (t) {
    var shadow = new index.ShadowBuilder();
    t.ok(shadow, 'Can create a shadow builder');
    t.end();
});

test('Test camera focus', function (t) {
    var camera = new THREE.PerspectiveCamera(30, 1.33, 0.1, 100000);
    camera.position.set(25, 100, 130);

    var geometry = new THREE.SphereBufferGeometry( 5000, 32, 32 );
    var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
    var sphere = new THREE.Mesh( geometry, material );
    sphere.position.set(25, 10000, 130);

    var controls = new index.EditorControls(camera, {addEventListener: function(){}});
    controls.focus(sphere, true);
    t.ok(camera.position.y > 0, 'Camera stays above ground');
    t.end();
});

test('Test scroll on click', function (t) {
    var camera = new THREE.PerspectiveCamera(30, 1.33, 0.1, 100000);
    camera.position.set(25, 100, 130);

    var geometry = new THREE.SphereBufferGeometry( 5000, 32, 32 );
    var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
    var sphere = new THREE.Mesh( geometry, material );
    sphere.position.set(25, 10000, 130);

    var onMouseDown = null;
    var onMouseWheel = null;
    new index.EditorControls(camera, {addEventListener: function(name, func){
        if (name === 'mousedown') {
            onMouseDown = func;
        } else if (name === 'mousewheel') {
            onMouseWheel = func;
        }
    }});
    var called = false;
    var preventDefault = function () {
        called = true;
    };
    onMouseWheel({
        preventDefault: preventDefault
    });
    t.ok(!called, 'No zoom on default');
    onMouseDown({
        button: 0,
        target: {
            getBoundingClientRect: function () { return {
                left: 0, top: 0, width: 1, height: 1
            };}
        },
        pageX: 0,
        pageY: 0
    });
    onMouseWheel({
        preventDefault: preventDefault
    });
    t.ok(called, 'Zoom after click');
    t.end();
});

test('Test sphere focus', function (t) {
    var camera = new THREE.PerspectiveCamera(30, 1.33, 0.1, 100000);
    camera.position.set(25, 100, 130);
    var center = new THREE.Vector3(0,-100,0);
    var sphere = new THREE.Sphere(center,0.1);
    var controls = new index.EditorControls(camera, {addEventListener: function(){}});
    controls.focus(sphere, true);
    t.ok(camera.position.distanceTo(center) < 1, 'Camera gets close');
    t.end();
});
