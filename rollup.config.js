import nodeResolve from 'rollup-plugin-node-resolve';
export default ({
    entry: 'src/index.js',
    external: ['three'],
    plugins: [nodeResolve({jsnext:true})]
});
