/**
 * This pass writes directly to the stencil buffer and does not modify
 * the color or depth buffers. It increments the stencil buffer for
 * each front-facing polygon and decrements it for each back facing polygon.
 */
'use strict';

export default function StencilPass ( scene, camera ) {

	this.scene = scene;
	this.camera = camera;

	this.enabled = true;
	this.clear = true;
	this.needsSwap = false;

	this.inverse = false;

}

StencilPass.prototype = {

	render: function ( renderer, writeBuffer, readBuffer ) {

		var context = renderer.context;

		// enable both front and back facing polygons
		context.disable(context.CULL_FACE);

		// enable depth test, pass for value < depth
		context.enable(context.DEPTH_TEST);
		context.depthFunc(context.LESS);

		// don't update color or depth mask
		context.colorMask( false, false, false, false );
		context.depthMask( false );
		context.stencilMask( 255 );

		// enable stencil test, clear buffer
		context.enable( context.STENCIL_TEST );
		context.clearStencil( 0 );
		context.stencilFunc( context.ALWAYS, 1, 0xffffffff );

		// deccrease stencil buffer for back-facing polygons on z fail
		context.stencilOpSeparate(context.BACK, context.KEEP, context.DECR_WRAP, context.KEEP);
		// increase stencil buffer for front-facing polygons on z fail
		context.stencilOpSeparate(context.FRONT, context.KEEP, context.INCR_WRAP, context.KEEP);

		// offset polygons to avoid self shadowing artifacts
		// TODO check if flickering at large distances can be improved with dynamic camera planes
		context.enable(context.POLYGON_OFFSET_FILL);
		context.polygonOffset(-0.01, 0.0);

		// draw into the stencil buffer
		renderer.render( this.scene, this.camera, readBuffer, this.clear );
		renderer.render( this.scene, this.camera, writeBuffer, this.clear );

		context.disable(context.POLYGON_OFFSET_FILL);

		// re-enable update of color and depth
		context.colorMask( true, true, true, true );
		context.depthMask( true );

		// only render where stencil is not zero
		context.stencilFunc( context.NOTEQUAL, 0, 0xffffffff );  // draw if != 0

		// keep stencilbuffer
		context.stencilOp( context.KEEP, context.KEEP, context.KEEP );
	}

};
