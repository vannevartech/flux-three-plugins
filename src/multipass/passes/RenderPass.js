/**
 * @author alteredq / http://alteredqualia.com/
 */
'use strict';
import * as THREE from 'three';

export default function RenderPass ( scene, camera, overrideMaterial, clearColor, clearAlpha ) {

	this.scene = scene;
	this.camera = camera;

	this.overrideMaterial = overrideMaterial;

	this.clearColor = clearColor;
	this.clearAlpha = ( clearAlpha !== undefined ) ? clearAlpha : 1;

	this.oldClearColor = new THREE.Color();
	this.oldClearAlpha = 1;

	this.enabled = true;
	this.clear = true;
	this.needsSwap = false;
	this.writeDepth = true;

}

RenderPass.prototype = {

	render: function ( renderer, writeBuffer, readBuffer ) {

		// enable or disable depth writing based on writeDepth
		if (!this.writeDepth) {
			renderer.context.depthMask( false );
			renderer.context.disable(renderer.context.DEPTH_TEST);
		} else {
			renderer.context.depthMask( true );
			renderer.context.enable(renderer.context.DEPTH_TEST);
		}

		this.scene.overrideMaterial = this.overrideMaterial;

		if ( this.clearColor ) {

			this.oldClearColor.copy( renderer.getClearColor() );
			this.oldClearAlpha = renderer.getClearAlpha();

			renderer.setClearColor( this.clearColor, this.clearAlpha );

		}

		renderer.render( this.scene, this.camera, readBuffer, this.clear );

		if ( this.clearColor ) {

			renderer.setClearColor( this.oldClearColor, this.oldClearAlpha );

		}

		this.scene.overrideMaterial = null;
		// reset depth mask
		renderer.context.depthMask( true );
		renderer.context.enable(renderer.context.DEPTH_TEST);

	}

};
