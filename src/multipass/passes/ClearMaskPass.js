'use strict';

export default function ClearMaskPass () {

    this.enabled = true;

}

ClearMaskPass.prototype = {

    render: function ( renderer ) {

        var context = renderer.context;

        context.disable( context.STENCIL_TEST );

    }

};
