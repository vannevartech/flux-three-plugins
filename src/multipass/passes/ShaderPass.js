/**
 * @author alteredq / http://alteredqualia.com/
 */
'use strict';
import * as THREE from 'three';

export default function ShaderPass ( shader, textureID ) {

	this.textureID = ( textureID !== undefined ) ? textureID : "tDiffuse";

	this.uniforms = THREE.UniformsUtils.clone( shader.uniforms );

	this.material = new THREE.ShaderMaterial( {

		defines: shader.defines || {},
		uniforms: this.uniforms,
		vertexShader: shader.vertexShader,
		fragmentShader: shader.fragmentShader

	} );

	this.renderToScreen = false;

	this.enabled = true;
	this.needsSwap = true;
	this.clear = false;
	// variable to control whether this pass modifies the depth buffer
	this.writeDepth = false;

	this.camera = new THREE.OrthographicCamera( -1, 1, 1, -1, 0, 1 );
	this.scene  = new THREE.Scene();

	this.quad = new THREE.Mesh( new THREE.PlaneBufferGeometry( 2, 2 ), null );
	this.scene.add( this.quad );

}

ShaderPass.prototype = {

	render: function ( renderer, writeBuffer, readBuffer ) {

		// enable or disable depth writing based on writeDepth
		if (!this.writeDepth) {
			renderer.context.depthMask( false );
			renderer.context.disable(renderer.context.DEPTH_TEST);
		} else {
			renderer.context.depthMask( true );
			renderer.context.enable(renderer.context.DEPTH_TEST);
		}

		if ( this.uniforms[ this.textureID ] ) {

			this.uniforms[ this.textureID ].value = readBuffer;

		}

		this.quad.material = this.material;

		if ( this.renderToScreen ) {

			renderer.render( this.scene, this.camera );

		} else {

			renderer.render( this.scene, this.camera, writeBuffer, this.clear );

		}

		// reset depth mask
		renderer.context.depthMask( true );
		renderer.context.enable(renderer.context.DEPTH_TEST);

	}

};
