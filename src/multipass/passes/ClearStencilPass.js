'use strict';

export default function ClearStencilPass () {

    this.enabled = true;

}

ClearStencilPass.prototype = {

    render: function ( renderer ) {

        var context = renderer.context;

        context.disable( context.STENCIL_TEST );
        renderer.clear(false, false, true);

    }

};
