/**
 * This shader takes each pixel and darkens it by a given
 * color and opacity. It performs the same function as the
 * 'Multiply' blend mode in Photoshop.
 *
 * darken = in * color;
 * out = alpha * darken + (1.0 - alpha) * in;
 */
'use strict';
import * as THREE from 'three';

var DarkenShader = {

	uniforms: {

		"tDiffuse":       { type: "t",  value: null },
		"alpha":          { type: "f",  value: 0.25 },
        "color":          { type: "v3", value: new THREE.Vector3(0.0, 0.0, 0.0) }
	},

	vertexShader: [

        "varying vec2 vUv;",

        "void main(void) {",

            "vUv = uv;",

            "vec4 p = modelViewMatrix * vec4( position, 1.0 );",
            "gl_Position = projectionMatrix * p;",
        "}"

	].join("\n"),

	fragmentShader: [

        "uniform sampler2D tDiffuse;",
        "uniform float alpha;",
        "uniform vec3 color;",

		"varying vec2 vUv;",

        "void main(void) {",
            "vec4 shadow = vec4(alpha) * vec4(color, 1.0) + vec4(1.0 - alpha);",
            "gl_FragColor = texture2D(tDiffuse, vUv) * shadow;",
        "}"

	].join("\n")

};

export default DarkenShader;
