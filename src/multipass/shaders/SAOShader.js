/**
 * Implements Scalable Ambient Obscurance (SAO) based on
 * http://graphics.cs.williams.edu/papers/SAOHPG12/ and
 * http://graphics.cs.williams.edu/papers/SAOHPG12/McGuire12SAO-talk.pdf
 *
 * SAO samples in a screen-space spiral pattern around the
 * current pixel, calculating the occlusion from each
 * sampled pixel and outputting a sum of the total occlusion.
 * It then uses an edge-aware blur to smooth the results
 */
'use strict';
import * as THREE from 'three';

var SAOShader = {

    uniforms: {

        // info from previous passes
        "tDiffuse":     { type: "t", value: null },
        "tDepth":       { type: "t", value: null },
        "tNorm":        { type: "t", value: null },
        "size":         { type: "v2", value: new THREE.Vector2( 512, 512 ) },

        // camera parameters
        "projInv":      { type: "m4", value: null },
        "near":         { type: "f", value: 1 },
        "far":          { type: "f", value: 600 },

        // occlsuion parameters
        "radius":       { type: "f", value: 2.0 },
        "bias":         { type: "f", value: 0.001 },
        "noise":        { type: "f", value: 0.05 },
        "intensity":    { type: "f", value: 7.5 },
        "sigma":        { type: "f", value: 0.05 },
        "projScale":    { type: "f", value: 0.03 },

        // which falloff function to use
        "variation":    { type: "i", value: 2 },

        // display only diffuse
        "onlyDiffuse":  { type: "i", value: 0 },
        // display only AO
        "onlyAO":       { type: "i", value: 0 }

    },

    // pass-through vertex shader
    vertexShader: [

        "varying vec2 vUv;",
        "varying mat3 viewInv;",

        "void main() {",
            "vUv = uv;",
            "viewInv = normalMatrix;",
            "gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
        "}"

    ].join("\n"),

    fragmentShader: [

        //"#extension GL_OES_standard_derivatives : enable", // TODO uncomment for THREE js r72

        "uniform sampler2D tDepth;",      // Depth texture
        "uniform sampler2D tNorm;",      // Normals texture
        "uniform sampler2D tDiffuse;",  // Diffuse rendering
        "uniform vec2 size;",

        // Camera parameters
        "uniform mat4 projInv;",          // Inverse of projection matrix
        "uniform float near;",            // Camera near plane
        "uniform float far;",            // Camera far plane

        // Occlusion parameters
        "uniform float radius;",        // World space sample radius
        "uniform float bias;",            // Bias to ignore AO in smooth corners
        "uniform float noise;",            // Amount of noise to use
        "uniform float intensity;",        // Intensity of AO
        "uniform float sigma;",            // Chosen for aesthetics
        "uniform float projScale;",        // A scale factor on the radius

        "uniform int variation;",        // Falloff function to use
        "uniform bool onlyAO;",          // Display only AO
        "uniform bool onlyDiffuse;",          // Display only diffuse

        "varying mat3 viewInv;",        // Inverse of view matrix
        "varying vec2 vUv;",

        "const int NUM_SAMPLES = 16;",
        "const int NUM_SPIRAL_TURNS = 7;",
        "const float PI = 3.14159;",

        /**
         * Pesudo random number generator
         *
         * @param  {vec2} coord     uv coordinates
         * @return {float}             random float between 0 and 1
         */
        "float rand( const vec2 coord ) {",
            "return fract(sin(dot(coord.xy ,vec2(12.9898,78.233))) * 43758.5453);",
        "}",

        /**
         * Read in depth from depth texture. The value is
         * split into the RGBA channels of the texture, so these
         * values must be bit-shifted back to obtain the depth.
         *
         * @param  {vec2} coord     screen-space coordinate
         * @return {float}             world-space z-depth
         */
        "float readDepth( in vec2 coord ) {",
            "vec4 rgba_depth = texture2D( tDepth, coord );",
            "const vec4 bit_shift = vec4( 1.0 / ( 256.0 * 256.0 * 256.0 ), 1.0 / ( 256.0 * 256.0 ), 1.0 / 256.0, 1.0 );",
            "float depth = dot( rgba_depth, bit_shift );",

            "return 2.0*near / ( far+near - depth*(far-near) );", // regular depth buffer
        "}",

        /**
         * Calculates the view-space (camera-space) position of
         * a given uv coordinate
         *
         * @param  {vec2} uv     screen-space coordinate
         * @return {vec3}         view-space coordinate
         */
        "vec3 calcVSPos(vec2 uv) {",
            "float depth = readDepth( uv );",
            "vec2 clipUV  = uv * 2.0 - vec2(1.0);", // convert to clip-space
            "vec4 invUV = (projInv * mat4(viewInv) * vec4(clipUV, -1.0, 1.0));", // invert view and camera projections
            "vec3 eyeRay = normalize( (invUV/invUV.w).xyz - cameraPosition);", // ray from camera

            "vec3 wsPos = eyeRay * depth + cameraPosition;", // world-space position
            "return (viewMatrix * vec4(wsPos, 1.0)).xyz;", // re-project world-space position to screen space
        "}",

        /**
         * Gets the screen-space location of a sample on a unit disk
         *
         * @param  {int} sampleNumber     the index of the current sample
         * @param  {float} spinAngle     the angle on the unit disk
         * @return {float} ssR             the screen space radius of the sample location
         * @return {vec2}                 the unit vector offset
         */
        "vec2 tapLocation(int sampleNumber, float spinAngle, out float ssR) {",
            "float alpha = (float(sampleNumber) + 0.5) / float(NUM_SAMPLES);",
            "float angle = alpha * (float(NUM_SPIRAL_TURNS) * 6.28) + spinAngle;",

            "ssR = alpha;",

            "return vec2(cos(angle), sin(angle));",
        "}",

        /**
         * Gets the view-space position of the point at screen-space pixel uv:
         * uv + unitOffset * ssR
         *
         * @param  {vec2} uv            screen-space coordinate
         * @param  {vec2} unitOffset    unit vector in offset direciton
         * @param  {float} ssR          screen-space radius
         * @return {vec3}               view-space position of point at uv
         */
        "vec3 getOffsetPt(vec2 uv, vec2 unitOffset, float ssR) {",
            "vec2 offsetUV = uv + ssR * unitOffset * (size.y / size.x);",
            "return calcVSPos(offsetUV);",
        "}",

        /**
         * Calculates the ambient occlusion from pixel uv from the
         * sample with index tapIndex
         *
         * @param  {vec2} uv                screen-space coordinate
         * @param  {vec3} vsPos             view-space position
         * @param  {vec3} vsNorm            view-space normal
         * @param  {float} sampleSSR         screen-space sampling radius
         * @param  {int} tapIndex              index of current sample
         * @param  {float} rotationAngle     angle of rotation on unit disk
         * @return {float}                   occlusion from this sample
         */
        "float sampleAO(vec2 uv, vec3 vsPos, vec3 vsNorm, float sampleSSR, int tapIndex, float rotationAngle) {",
            "const float epsilon = 0.01;",
            "float r2 = radius * radius;",

            // offset on the unit disk, spun for this pixel
            "float ssR;",
            "vec2 unitOffset = tapLocation(tapIndex, rotationAngle, ssR);",
            "ssR *= sampleSSR;",

            // view-space coordinate of sample point
            "vec3 Q = getOffsetPt(uv, unitOffset, ssR);",
            "vec3 v = Q - vsPos;",

            "float vv = dot(v, v) / projScale;",
            "float vn = dot(v, vsNorm);",

            "if (variation == 1) {",
                 // Smoother transition to zero (lowers contrast, smoothing out corners)
                 "float f = max(r2 - vv, 0.0);",
                 "return f * f * f * max( (vn - bias) / (epsilon + vv), 0.0);",
             "} else if (variation == 2) {",
                 // Medium contrast (which looks better at high radii), no division.
                 "float invR2 = 1.0 / r2;",
                 "return 4.0 * max(1.0 - vv * invR2, 0.0) * max(vn, 0.0);",
             "} else if (variation == 3) { ",
                 // Low contrast, no division operation
                 "return 2.0 * float(vv < r2) * max(vn, 0.0);",
             "} else {",
                 // Default to variation == 0
                 // From HPG12 paper (large epsilon to avoid overdarkening within cracks)
                 "return float(vv < r2) * max(vn / (epsilon + vv), 0.0);",
            "}",

        "}",

        /**
         * Calculates the total occlusion of pixel uv by sampling
         * nearby pixels and summing the occlusion from each
         *
         * @param  {vec2} uv       texture coordinate of current pixel
         * @return {float}        occlusion of pixel uv in [0, 1]
         */
        "float calcAO(vec2 uv) {",
            "vec3 vsPos = calcVSPos(uv);",
            "vec3 vsNorm = texture2D( tNorm, uv).rgb;",

            "float sampleNoise = rand(uv) * noise;",
            "float rAngle = 2.0 * PI * sampleNoise;", // random angle

            "float ssR = projScale * radius / ( vsPos.z ) ;", // radius of influence in screen space

            // sum occlusion from each sample and average
            "float occlusion = 0.0;",
            "for (int i = 0; i < NUM_SAMPLES; ++i) {",
                "occlusion += sampleAO(uv, vsPos, vsNorm, ssR, i, rAngle);",
            "}",
            "occlusion = (1.0 - occlusion * (2.0*sigma / float(NUM_SAMPLES)));",
            "occlusion = clamp(pow(occlusion, 1.0 + intensity), 0.0, 1.0);",

            // TODO uncomment for THREE js r72
            // slight blur (from paper), effects are subtle
            /*"if (abs(dFdx(vsPos.z)) < 0.02) {",
                "occlusion -= dFdx(occlusion) * ((uv.x) - 0.5);",
            "}",
            "if (abs(dFdy(vsPos.z)) < 0.02) {",
                "occlusion -= dFdy(occlusion) * ((uv.y) - 0.5);",
            "}",*/

            "return occlusion;",
        "}",

        "void main(void) {",
            // diffuse color of pixel
            "vec4 color = texture2D( tDiffuse, vUv );",

            "if (onlyDiffuse) {",
                "gl_FragColor = vec4( color.rgb, 1.0 );",
            "} else {",
                "float occlusion = calcAO( vUv );",
                "if (onlyAO) {",
                    "gl_FragColor = vec4( vec3(occlusion), 1.0 );",
                "} else {",
                    "gl_FragColor = vec4( vec3(occlusion)*color.rgb, 1.0 );",
                "}",
            "}",

        "}"



    ].join("\n")

};

export default SAOShader;
