/**
 * @author qiao / https://github.com/qiao
 * @author mrdoob / http://mrdoob.com
 * @author alteredq / http://alteredqualia.com/
 * @author WestLangley / http://github.com/WestLangley
 * @author arodic / http://akirodic.com/
 */
'use strict';
import * as THREE from 'three';

// Singleton used to track current editor for scroll event capture.
var _currentEditor = null;

export default function EditorControls( object, domElement, center ) {

	domElement = ( domElement !== undefined ) ? domElement : document;

	// API

	this.enabled = true;
	this.center = center = center || new THREE.Vector3();

	// internals

	var scope = this;
	var vector = new THREE.Vector3();
	var matrix = new THREE.Matrix3();

	var STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2 };
	var state = STATE.NONE;
	var parentRect = null;

	// pointer data

	var touches = [];

	// pointers are expressed in -1 to 1 coordinate space relative to domElement.

	var pointers = [ new THREE.Vector2(), new THREE.Vector2() ];
	var pointersOld = [ new THREE.Vector2(), new THREE.Vector2() ];
	var pointersDelta = [ new THREE.Vector2(), new THREE.Vector2() ];

	// events

	var changeEvent = { type: 'change' };

	var ORTHO_FRUSTUM_WIDTH = 1e6;
	var PERSP_FRUSTUM_WIDTH = 1e3;
	var ZOOM_MAX_EXTENTS = 1e18;
	var MIN_FOCUS_SIZE = 1e-3;
	var EPS = 1e-6;
	var CAMERA_PADDING = 1.2;

	// hepler functions

	var getClosestPoint = function( point, pointArray ) {

		if ( pointArray[ 0 ].distanceTo( point) < pointArray[ 1 ].distanceTo( point) ) {

			return pointArray[ 0 ];

		}

		return pointArray[ 1 ];

	};

	var setPointers = function( event ) {

		// Set pointes from mouse/touch events and convert to -1 to 1 coordinate space.
		if (!parentRect || event.type === 'mousedown') {
			// Cache the parent rect at the beginning of a drag event sequence
			parentRect = event.target.getBoundingClientRect();
		}
		// Filter touches that originate from the same element as the event.

		touches.length = 0;

		if ( event.touches ) {
			for ( var i = 0; i < event.touches.length; i++ ) {
				if ( event.touches[ i ].target === event.target ) {
					touches.push( event.touches[ i ] );
				}
			}
		}

		// Set pointer[0] from mouse event.clientX/Y

		if ( touches.length === 0 ) {
			// Compute the position on the page with a relative scale from the element
			pointers[ 0 ].set(
				( event.pageX - parentRect.left ) / parentRect.width * 2 - 1,
				( event.pageY - parentRect.top ) / parentRect.height * 2 - 1
			);

		// Set both pointer[0] and pointer[1] from a single touch.

		} else if ( touches.length == 1 ) {

			pointers[ 0 ].set(
				( touches[ 0 ].pageX - parentRect.left ) / parentRect.width * 2 - 1,
				( touches[ 0 ].pageY - parentRect.top ) / parentRect.height * 2 - 1
			);
			pointers[ 1 ].copy( pointers[ 0 ] );

		// Set pointer[0] and pointer[1] from two touches.

		} else if ( touches.length == 2 ) {

			pointers[ 0 ].set(
				( touches[ 0 ].pageX - parentRect.left ) / parentRect.width * 2 - 1,
				( touches[ 0 ].pageY - parentRect.top ) / parentRect.height * 2 - 1
			);
			pointers[ 1 ].set(
				( touches[ 1 ].pageX - parentRect.left ) / parentRect.width * 2 - 1,
				( touches[ 1 ].pageY - parentRect.top) / parentRect.height * 2 - 1
			);

		}

		// Clear the cached bounds at the end of a drag event sequence
		if (parentRect && event.type === 'mouseup') {
			parentRect = null;
		}
	};

	/**
	 * Update camera clipping planes based on size of scene
	 * @param  {THREE.camera} camera Perspective camera object
	 * @param  {Number} radius Size of the scene
	 */
	this.updateClippingPersp = function updateClippingPersp (camera, radius) {
		var factor = PERSP_FRUSTUM_WIDTH;
		camera.near = radius / factor;
		camera.far = radius * factor + factor;
	};

	/**
	 * Update camera clipping planes based on size of scene
	 * @param  {THREE.camera} camera Orthographic camera object
	 * @param  {Number} radius Size of the scene
	 */
	this.updateClippingOrtho = function updateClippingOrtho (camera, radius) {
		var nearFarDistance = ORTHO_FRUSTUM_WIDTH;
		var factor = Math.max(2*radius,nearFarDistance);
		camera.near = -1 * factor;
		camera.far = factor;
	};


	this.focus = function focus ( target, frame ) {
		// Collection of all centers and radii in the hierarchy of the target.

		var targets = [];

		// Bounding box (minCenter/maxCenter) encompassing all centers in hierarchy.

		var minCenter;
		var maxCenter;

		if (target.constructor !== THREE.Sphere) {// Object3D
			target.traverse( function( child ) {

				if (child.visible) {

					child.updateMatrixWorld( true );

					var center = new THREE.Vector3();
					var scale = new THREE.Vector3();
					var radius = 0;

					child.matrixWorld.decompose( center, new THREE.Quaternion(), scale );
					scale = ( scale.x + scale.y + scale.z ) / 3;

					//TODO: make work with non-uniform scale

					if ( child.geometry ) {

						child.geometry.computeBoundingSphere();
						center.copy( child.geometry.boundingSphere.center.clone()
									.applyMatrix4(child.matrixWorld) );
						radius = child.geometry.boundingSphere.radius * scale;

					}

					if ( !frame || child.geometry ) {

						targets.push( { center: center, radius: radius } );

						if ( !minCenter ) minCenter = center.clone();
						if ( !maxCenter ) maxCenter = center.clone();

						minCenter.min( center );
						maxCenter.max( center );

					}

				}

			} );

			// Bail if there is not visible geometry
			if (!minCenter || !maxCenter) return;

			// Center of the bounding box.

			var cumulativeCenter = minCenter.clone().add( maxCenter ).multiplyScalar( 0.5 );

			// Furthest ( center distance + radius ) from CumulativeCenter.

			var cumulativeRadius = 0;

			targets.forEach( function( child ) {

				var radius = cumulativeCenter.distanceTo( child.center ) + child.radius;
				cumulativeRadius = Math.max( cumulativeRadius, radius );

			} );
		} else { // Sphere
			cumulativeRadius = target.radius;
			cumulativeCenter = target.center;
		}

		// When the radius is nearly zero fall back to a reasonable view size
		// This works well for single points which have a minimum pixel size
		// so they will still be visible, even though they are very small relative to the focus size
		if (cumulativeRadius < EPS) {
			cumulativeRadius = MIN_FOCUS_SIZE;
		}

		if ( object instanceof THREE.PerspectiveCamera ) {
			// Use the vector from center to camera to reposition
			vector.copy(object.position).sub(center);
			object.position.copy(cumulativeCenter).add(vector);

			// Look towards cumulativeCenter
			center.copy( cumulativeCenter );
			object.lookAt( center );

			if ( frame && cumulativeRadius ) {

				// Adjust distance to frame cumulativeRadius

				var fovFactor = Math.tan( ( object.fov / 2 ) * Math.PI / 180.0 );
				var pos = object.position.clone().sub( center ).normalize().multiplyScalar( CAMERA_PADDING * cumulativeRadius  / fovFactor );

				object.position.copy( center ).add( pos );
				this.updateClippingPersp(object, cumulativeRadius);
			}

		} else if ( object instanceof THREE.OrthographicCamera ) {

			// Align camera center with cumulativeCenter
			center.copy( cumulativeCenter );
			object.position.copy( center );

			if ( frame && cumulativeRadius ) {
				// Offset camera by radius along camera axis so it's outside the object
				// If camera is too close to geometry, point distance attenuation looks bad
				var z = new THREE.Vector3(0,0,1);
				z.applyQuaternion(object.quaternion);
				z.multiplyScalar(2*cumulativeRadius+1);
				object.position.sub(z);

				// Adjust camera boundaries to frame cumulativeRadius

				var cw = object.right - object.left;
				var ch = object.top - object.bottom;
				var aspect = Math.abs(cw / ch);
				var radius = CAMERA_PADDING * cumulativeRadius;
				if ( aspect < 1 ) {

					object.top = Math.sign(object.top) * radius / aspect;
					object.right = Math.sign(object.right) * radius;
					object.bottom = Math.sign(object.bottom) * radius / aspect;
					object.left = Math.sign(object.left) * radius;

				} else {

					object.top = Math.sign(object.top) * radius;
					object.right = Math.sign(object.right) * radius * aspect;
					object.bottom = Math.sign(object.bottom) * radius;
					object.left = Math.sign(object.left) * radius * aspect;

				}
				this.updateClippingOrtho(object, cumulativeRadius);

			}

		}

		scope.dispatchEvent( changeEvent );

	};

	this.pan = function ( delta ) {

		var distance = object.position.distanceTo( center );

		vector.set( -delta.x, delta.y, 0 );

		if ( object instanceof THREE.PerspectiveCamera ) {

			var fovFactor = distance * Math.tan( ( object.fov / 2 ) * Math.PI / 180.0 );
			vector.multiplyScalar( fovFactor );
			vector.x *= object.aspect;

		} else if ( object instanceof THREE.OrthographicCamera ) {

			vector.x *= ( object.right - object.left ) / 2;
			vector.y *= ( object.top - object.bottom ) / 2;

		}

		vector.applyMatrix3( matrix.getNormalMatrix( object.matrix ) );
		object.position.add( vector );
		center.add( vector );

		scope.dispatchEvent( changeEvent );

	};

	this.zoom = function ( delta ) {

		if ( object instanceof THREE.PerspectiveCamera ) {

			var distance = object.position.distanceTo( center );

			vector.set( 0, 0, delta.y );

			vector.multiplyScalar( distance );

			vector.applyMatrix3( matrix.getNormalMatrix( object.matrix ) );

			// Clamp the length to not be too big
			vector.setLength(Math.min(vector.length(), distance*0.5));

			var newDistance = object.position.clone().add(vector).distanceTo( center );
			// check if we should be moving closer but are going to pass the target or
			// we are moving farther, but way too far away
			if ( (delta.y < 0 && newDistance >= distance) || (delta.y > 0 && newDistance > ZOOM_MAX_EXTENTS)) {
				return;
			}
			object.position.add( vector );
			this.updateClippingPersp(object, newDistance);

		} else if ( object instanceof THREE.OrthographicCamera ) {
			// Prevent crazy fast zoom, and never scale by a negative number!
			var dy = Math.min(2,Math.max(0.5,1+delta.y));
			object.top *= dy;
			object.right *= dy;
			object.bottom *= dy;
			object.left *= dy;

		}

		scope.dispatchEvent( changeEvent );

	};

	this.rotate = function ( delta ) {

		vector.copy( object.position ).sub( center );

		var theta = Math.atan2( vector.x, vector.y );
		var phi = Math.atan2( Math.sqrt( vector.x * vector.x + vector.y * vector.y ), vector.z );

		theta += delta.x;
		phi -= delta.y;

		phi = Math.max( EPS, Math.min( Math.PI - EPS, phi ) );

		var radius = vector.length();

		vector.x = radius * Math.sin( phi ) * Math.sin( theta );
		vector.y = radius * Math.sin( phi ) * Math.cos( theta );
		vector.z = radius * Math.cos( phi );

		object.position.copy( center ).add( vector );

		object.lookAt( center );

		scope.dispatchEvent( changeEvent );

	};

	// export center so it can be restored later
	this.toJSON = function () {
		return {
			cx: center.x,
			cy: center.y,
			cz: center.z
		};
	};

	// restore the center point from previous data
	this.fromJSON = function (data) {
		this.center.x = data.cx;
		this.center.y = data.cy;
		this.center.z = data.cz;
	};

	// mouse

	function onMouseDown( event ) {

		_currentEditor = this;

		if ( scope.enabled === false ) return;

		if ( event.button === 0 ) {

			state = STATE.ROTATE;

			if ( object instanceof THREE.OrthographicCamera ) {

				state = STATE.PAN;

			}

		} else if ( event.button === 1 ) {

			state = STATE.ZOOM;

		} else if ( event.button === 2 ) {

			state = STATE.PAN;

		}

		setPointers( event );

		pointersOld[ 0 ].copy( pointers[ 0 ] );

		// Camera navigation continues while the user drags anywhere on the page
		document.addEventListener( 'mousemove', onMouseMove, false );
		document.addEventListener( 'mouseup', onMouseUp, false );
		document.addEventListener( 'dblclick', onMouseUp, false );

	}

	function onMouseMove( event ) {

		if ( scope.enabled === false ) return;

		setPointers( event );

		pointersDelta[ 0 ].subVectors( pointers[ 0 ], pointersOld[ 0 ] );
		pointersOld[ 0 ].copy( pointers[ 0 ] );

		if ( state === STATE.ROTATE ) {

			scope.rotate( pointersDelta[ 0 ] );

		} else if ( state === STATE.ZOOM ) {

			scope.zoom( pointersDelta[ 0 ] );

		} else if ( state === STATE.PAN ) {

			scope.pan( pointersDelta[ 0 ] );

		}

	}

	function onMouseUp( ) {
		// Make sure to catch the end of the drag even outside the viewport
		document.removeEventListener( 'mousemove', onMouseMove, false );
		document.removeEventListener( 'mouseup', onMouseUp, false );
		document.removeEventListener( 'dblclick', onMouseUp, false );

		state = STATE.NONE;

	}

	function onMouseWheel( event ) {

		if (_currentEditor !== this || scope.enabled === false ) return;

		event.preventDefault();

		var delta = 0;

		if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9

			delta = - event.wheelDelta;

		} else if ( event.detail ) { // Firefox

			delta = event.detail * 10;

		}

		scope.zoom( new THREE.Vector2( 0, delta / 1000 ) );

	}

	// Camera navigation begins when the user clicks inside the dom element
	domElement.addEventListener( 'contextmenu', function ( event ) { event.preventDefault(); }, false );
	domElement.addEventListener( 'mousedown', onMouseDown, false );
	domElement.addEventListener( 'mousewheel', onMouseWheel, false );
	domElement.addEventListener( 'DOMMouseScroll', onMouseWheel, false ); // firefox

	function touchStart( event ) {

		event.preventDefault();

		if ( scope.enabled === false ) return;

		setPointers( event );

		pointersOld[ 0 ].copy( pointers[ 0 ] );
		pointersOld[ 1 ].copy( pointers[ 1 ] );

	}


	function touchMove( event ) {

		event.preventDefault();

		if ( scope.enabled === false ) return;

		setPointers( event );

		switch ( touches.length ) {

			case 1:
				pointersDelta[ 0 ].subVectors( pointers[ 0 ], getClosestPoint( pointers[ 0 ], pointersOld ) );
				pointersDelta[ 1 ].subVectors( pointers[ 1 ], getClosestPoint( pointers[ 1 ], pointersOld ) );

				if ( object instanceof THREE.PerspectiveCamera ) {

					scope.rotate( pointersDelta[ 0 ] );

				} else if ( object instanceof THREE.OrthographicCamera ) {

					scope.pan( pointersDelta[ 0 ] );

				}
				break;

			case 2:
				pointersDelta[ 0 ].subVectors( pointers[ 0 ], getClosestPoint( pointers[ 0 ], pointersOld ) );
				pointersDelta[ 1 ].subVectors( pointers[ 1 ], getClosestPoint( pointers[ 1 ], pointersOld ) );

				var prevDistance = pointersOld[ 0 ].distanceTo( pointersOld[ 1 ] );
				var distance = pointers[ 0 ].distanceTo( pointers[ 1 ] );

				if ( prevDistance ) {

					scope.zoom( new THREE.Vector2(0, prevDistance - distance ) );
					scope.pan( pointersDelta[ 0 ].clone().add( pointersDelta[ 1 ] ).multiplyScalar(0.5) );

				}
				break;
		}

		pointersOld[ 0 ].copy( pointers[ 0 ] );
		pointersOld[ 1 ].copy( pointers[ 1 ] );

	}

	function touchEnd( event ) {
		event.preventDefault();
	}

	domElement.addEventListener( 'touchstart', touchStart, false );
	domElement.addEventListener( 'touchmove', touchMove, false );
	domElement.addEventListener( 'touchend', touchEnd, false );

}

EditorControls.prototype = Object.create( THREE.EventDispatcher.prototype );
EditorControls.prototype.constructor = EditorControls;
