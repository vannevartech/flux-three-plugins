/**
 * ShadowBuilder is defined for each shadow-casting light
 * in the scene. It holds a list of geometries and the shadow
 * volume (from this light) for each geometry.
 *
 * To calculate shadows, a silhouette of the object is
 * generated. Then, the silhouette is extruded in the
 * direction of the light. Finally, the shadow caps are
 * added to the volume.
 *
 * Precondition: all triangles are defined clockwise.
 *
 * Currently supports: directional lights, point lights.
 *
 */
'use strict';
import * as THREE from 'three';

export default function ShadowBuilder ( position, category ) {
    /**
     * The origin of the light that is casting shadows.
     * Directional lights are always pointing at the origin
     *
     * @type {THREE.Vector3}
     */
    this.origin = position;

    /**
     * The type of the light source, 'directional' or 'point'
     *
     * @type {String}
     * @default 'directional'
     */
    this.category = 'directional';
    if (category) this.category = category;

    /**
     * Dictionary mapping the unique id of a THREE.Mesh (uuid)
     * to its shadow volume
     *
     * @type {Object}
     */
    this.meshes = {};

    /**
     * Default material for all shadow volumes. The only
     * requirement is that it is double-sided.
     *
     * @type {THREE.Material}
     */
    this.material = new THREE.MeshLambertMaterial( { side: THREE.DoubleSide } );

    /**
     * Extent of the shadow volume, based on the camera
     * far plane. If the back of the volume is cut off, there
     * will be shadow artifacts.
     *
     * TODO: get from camera. (Should be slightly less than far plane)
     *
     * @type {Number}
     */
    this.far = 10000.0 * 0.75;

    /**
     * Offsets the shadow volume to prevent z-fighting where
     * volume faces are coincident with geometry. High bias
     * causes bleeding, low bias causes z-fighting.
     *
     * @type {Number}
     */
    this.bias = 0.0075;
}

ShadowBuilder.prototype = {

    // INITIALIZATION

    /**
     * Adds a mesh (THREE.Mesh) to the shadow builder.
     * Creates a dictionary entry for this mesh, holding geometry
     * information and shadow volume information.
     *
     * @param {THREE.Mesh} mesh      the mesh casting the shadow
     */
    _addMesh: function ( mesh ) {
        this.meshes[mesh.uuid] = {
            'v' : null,
            'f' : null,

            'matrixWorld' : new THREE.Matrix4(),
            'normalMatrix' : new THREE.Matrix3(),

            'vertices': [],     // holds vertices (THREE.Vector3) of shadow volume
            'faces': [],        // holds faces (THREE.Face3) of shadow volume
            'volume': null      // shadow volume
        };

        mesh.updateMatrixWorld(true);
        // mesh transform matrix
        this.meshes[mesh.uuid].matrixWorld = mesh.matrixWorld;
        // inverse transpose of upper 3x3 for normal transforms
        this.meshes[mesh.uuid].normalMatrix.getNormalMatrix(mesh.matrixWorld);

        this._addGeometry(mesh);

        var volume = new THREE.Mesh(new THREE.Geometry(), this.material);
        volume.geometry.vertices = this.meshes[mesh.uuid].vertices;
        volume.geometry.faces = this.meshes[mesh.uuid].faces;
        this.meshes[mesh.uuid].volume = volume;
    },

    /**
     * Transforms and adds vertex and face info from a mesh.
     *
     * @param {THREE.Mesh} mesh      the mesh casting the shadow
     */
    _addGeometry: function ( mesh ) {
        // the BufferGeometry holds vertices and faces in a different format
        var geom;
        if (mesh.geometry instanceof THREE.BufferGeometry) {
            geom = new THREE.Geometry().fromBufferGeometry( mesh.geometry );
        } else {
            geom = mesh.geometry.clone(); // TODO any way to avoid cloning entire geom?
        }
        geom.computeFaceNormals();
        geom.mergeVertices();

        // input geometry values
        var v = geom.vertices;
        var f = geom.faces;

        // transform vertices by mesh scale/rotate/translate
        for (var i = 0; i < v.length; i++) {
            v[i].applyMatrix4(this.meshes[mesh.uuid].matrixWorld);
        }

        this.meshes[mesh.uuid].v = v;
        this.meshes[mesh.uuid].f = f;

        geom.dispose();
    },

    // GEOMETRY COMPUTATION

    /**
     * Deletes the current volume geometry if it exists,
     * then computes the silhouette, calculates the shadow
     * volume and the THREE.js mesh representing it.
     *
     * @param  {String} meshID       unique id of mesh
     */
    _computeShadowVolume: function ( meshID ) {
        // clear previous volume calculation
        this.meshes[meshID].vertices.length = 0;
        this.meshes[meshID].faces.length = 0;
        if (this.meshes[meshID].volume) {
            this.meshes[meshID].volume.geometry.dispose();
        }

        var edges = this._getSilhouetteAndCap(meshID);
        this._computeShadowSides(meshID, edges);

        var volume = this.meshes[meshID].volume;
        volume.geometry = new THREE.Geometry();
        volume.geometry.vertices = this.meshes[meshID].vertices;
        volume.geometry.faces = this.meshes[meshID].faces;

        this.meshes[meshID].volume.geometry.computeFaceNormals();
    },

    /**
     * Extrudes the silhouette in the direction of the light,
     * adds these triangles to the list of vertices and faces.
     *
     * @param  {String} meshID       unique id of mesh
     * @param  {String[]} edges      array of edges in silhouette
     */
    _computeShadowSides: function ( meshID, edges ) {
        // generates two triangles for each side
        for (var i = 0; i < edges.length; i++) {
            var a = this._projectVertex(this.meshes[meshID].v[edges[i][0]], this.bias);
            var b = this._projectVertex(this.meshes[meshID].v[edges[i][1]], this.bias);
            var a_prime = this._projectVertex(a, this.far);
            var b_prime = this._projectVertex(b, this.far);
            this._addQuad(meshID, a, a_prime, b, b_prime);
        }
    },

    /**
     * Calculates the silhouette of the geometry. Each edge is added
     * to the dictionary if not yet present, and the appropriate boolean
     * (light-facing or not-light-facing) is set to be true. An edge
     * from vertex a to vertex b is represented as the string 'a_b'
     * ('a_b' == 'b_a') plus the array [a, b].
     *
     * While iterating through the faces, this function
     * also computes the back and front caps of the shadow volume.
     *
     * @param  {String} meshID       unique id of mesh
     * @return {Array.<Array.<Number>>}     array of edges in silhouette
     */
    _getSilhouetteAndCap: function ( meshID ) {
        var edgeDict = {}; // edges currently in silhouette
        var edges = []; // final silhouette edges
        var i, j, face, verts, isFacing, a, b;

        // iterate over all triangles
        var f = this.meshes[meshID].f;
        var v = this.meshes[meshID].v;
        for (i = 0; i < f.length; i++) {
            face = f[i];
            verts = [face.a, face.b, face.c];

            // check if triangle is light-facing
            isFacing = this._isLightFacing( meshID, face );
            for (j = 0; j < 3; j++) {
                a = verts[j];
                b = verts[(j+1)%3];
                this._addEdge( edgeDict, a, b, isFacing );
            }

            // add non-light-facing triangles as caps
            if (!isFacing) {
                this._addCapTriangle(meshID, v[face.a], v[face.c], v[face.b]);
            }
        }

        var manifold = true;
        for (var k in edgeDict) {
            // first check if edge is manifold
            if (!edgeDict[k].manifold) {
                manifold = false;
                break;
            } else if (edgeDict[k].front && edgeDict[k].back) {
                // all edges on the border between light-facing and
                // back-facing are part of the silhouette
                var endpts = edgeDict[k].verts;
                edges.push(endpts);
            }
        }

        // for non-manifold geometries, add all edges to the silhouette
        if (!manifold) {
            edges.length = 0;
            for (i = 0; i < f.length; i++) {
                face = f[i];
                verts = [face.a, face.b, face.c];

                isFacing = this._isLightFacing( meshID, face );
                for (j = 0; j < 3; j++) {
                    a = verts[j];
                    b = verts[(j+1)%3];
                    var edge = !isFacing ? [b, a] : [a, b];
                    edges.push(edge);
                }

                // add remaining triangles to caps
                if (isFacing) {
                    this._addCapTriangle(meshID, v[face.a], v[face.b], v[face.c]);
                }
            }
        }

        return edges;
    },

    // MATH FUNCTIONS

    /**
     * Check if triangle faces the light. For point lights, calculate
     * a ray from the light origin to the center of the triangle. For
     * directional lights use a ray in the light direction.
     *
     * @param  {string} meshID       unique id of mesh
     * @param  {THREE.Face3} face    the current face
     * @return {bool}                true if triangle faces the light
     */
    _isLightFacing: function ( meshID, face ) {
        var n = face.normal.clone().applyMatrix3(this.meshes[meshID].normalMatrix);

        if (this.category == 'point') {
            var v = this.meshes[meshID].v;
            var a = v[face.a];
            var b = v[face.b];
            var c = v[face.c];
            var center = new THREE.Vector3();
            center.x = (a.x + b.x + c.x)/3.0;
            center.y = (a.y + b.y + c.y)/3.0;
            center.z = (a.z + b.z + c.z)/3.0;
            var dir = this.origin.clone().sub(center);
            return ( n.dot(dir) >= 0 );
        } else {
            return ( n.dot(this.origin) >= 0 );
        }

    },

    /**
     * Projects a vertex in the direction of the light for a length
     * of dist. For point lights, project from light origin,
     * for directional lights, project along light direction.
     *
     * @param  {THREE.Vector3} v     vertex to be projected
     * @param  {Number} dist         distance to project
     * @return {THREE.Vector3}       new, projected vertex
     */
    _projectVertex: function ( v, dist ) {
        var v_prime = v.clone();

        if (this.category == 'point') {
            var dir = v.clone().sub(this.light).normalize();
            v_prime.add(dir.multiplyScalar(dist));
        } else {
            var l = this.origin.clone().normalize();
            l.negate().multiplyScalar(dist);
            v_prime.add(l);
        }

        return v_prime;
    },

    // GEOMETRY ADDITION

    /**
     * Attempts to add the edge from vertex a to b. Edges are
     * defined as a string 'a_b'. Removes edge from dict if
     * either 'a_b' or 'b_a' is already present.
     *
     * @param {Object} edgeDict      dictionary of processed edges
     * @param {int} a                index of vertex a
     * @param {int} b                index of vertex b
     * @param {bool} isFacing        is the edge from a light-facing triangle
     */
    _addEdge: function ( edgeDict, a, b, isFacing ) {
        var name = a+'_'+b;
        if (edgeDict[b+'_'+a]) name = b+'_'+a;

        if ( edgeDict[name] ) {
            // check if we've already seen the edge twice
            if (edgeDict[name].front == edgeDict[name].back) {
                edgeDict[name].manifold = false;
                return;
            }
            // otherwise flip the appropriate boolean
            if (isFacing) {
                edgeDict[name].front = !edgeDict[name].front;
            } else {
                edgeDict[name].back = !edgeDict[name].back;
            }
            edgeDict[name].manifold = true;
        } else {
            var verts = !isFacing ? [b, a] : [a, b];
            edgeDict[a+'_'+b] = {
                'verts'     : verts,
                'front'     : isFacing,
                'back'      : !isFacing,
                'manifold'  : false
            };
        }
    },

    /**
     * Adds a triangle to the front and back shadow caps.
     *
     * @param {String} meshID        unique id of mesh
     * @param {[type]} a             vertex of triangle
     * @param {[type]} b             vertex of triangle
     * @param {[type]} c             vertex of triangle
     */
    _addCapTriangle: function( meshID, a, b, c ) {
        // front cap
        var a_front = this._projectVertex(a, this.bias);
        var b_front = this._projectVertex(b, this.bias);
        var c_front = this._projectVertex(c, this.bias);
        this._addTriangle(meshID, a_front, b_front, c_front);

        // back cap
        var a_back = this._projectVertex(a, this.far);
        var b_back = this._projectVertex(b, this.far);
        var c_back = this._projectVertex(c, this.far);
        this._addTriangle(meshID, a_back, c_back, b_back);
    },

    /**
     * Adds two triangles defining the rectangular shadow side abcd.
     *
     * @param {String} meshID               unique id of mesh
     * @param {THREE.Vector3} a             vertex of rectangle
     * @param {THREE.Vector3} b             vertex of rectangle
     * @param {THREE.Vector3} c             vertex of rectangle
     * @param {THREE.Vector3} d             vertex of rectangle
     */
    _addQuad: function ( meshID, a, b, c, d ) {
        this._addTriangle(meshID, a, b, c);
        this._addTriangle(meshID, d, c, b);
    },

    /**
     * Adds the vertices and approrpiate face to shadow volume.
     *
     * @param {String} meshID               unique id of mesh
     * @param {THREE.Vector3} a             vertex of triangle
     * @param {THREE.Vector3} b             vertex of triangle
     * @param {THREE.Vector3} c             vertex of triangle
     */
    _addTriangle: function ( meshID, a, b, c ) {
        this.meshes[meshID].vertices.push(a);
        this.meshes[meshID].vertices.push(b);
        this.meshes[meshID].vertices.push(c);
        var l = this.meshes[meshID].vertices.length;
        this.meshes[meshID].faces.push(new THREE.Face3(l-3, l-2, l-1));
    },


    // PUBLIC FUNCTIONS

    /**
     * Calculates shadow volume if not yet calculated, returns volume
     *
     * @param  {THREE.Mesh} mesh     position of the light
     * @return {THREE.Mesh}          a mesh representing the shadow volume
     */
    getShadowVolume: function ( mesh ) {
        if (!this.meshes[mesh.uuid]) {
            this._addMesh(mesh);
            this._computeShadowVolume(mesh.uuid);
        }

        return this.meshes[mesh.uuid].volume;
    },

    /**
     * Recalculates a shadow volume (used when light or geometry changes)
     *
     * @param  {THREE.Mesh} mesh     mesh casting the shadow
     * @return {THREE.Mesh}          a mesh representing the shadow volume
     */
    updateShadowVolume: function ( mesh ) {
        mesh.updateMatrixWorld(true);
        this.meshes[mesh.uuid].matrixWorld = mesh.matrixWorld;
        this.meshes[mesh.uuid].normalMatrix.getNormalMatrix(mesh.matrixWorld);

        this._addGeometry( mesh );
        this._computeShadowVolume( mesh.uuid );
        return this.meshes[mesh.uuid].volume;
    },

    /**
     * Updates the position of the light
     *
     * @param  {THREE.Vector3} position     position of the light
     */
    updateLight: function ( position ) {
        this.origin = position;
    }
};